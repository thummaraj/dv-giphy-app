import React from 'react';
import { Card } from 'antd'
import { connect} from 'react-redux'
const {Meta} = Card;



const mapDispathToProps = dispath => {
    return {
      onItemGiphyClick: item =>
      dispath({
        type: 'click_item',
        payload: item
      })
    }
  }

function ItemGiphy(props) {
    console.log('items:', props.items);
    const item = props.item
    return (

        <Card 
        onClick={ () => {
          props.onItemGiphyClick(item);
        }}
        style={{ height: 300}}
        
        hoverable
        cover={< img src={item.images.fixed_width.url} />}
        >
        <Meta
        title={item.title}
        />
        </Card>
    

    )
}
export default connect(
    null,
    mapDispathToProps)(ItemGiphy)