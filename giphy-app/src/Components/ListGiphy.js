import React from "react";
import ItemGiphy from "./ItemGiphy";
import { List } from "antd";

function ListGiphy(props) {
  return (
    <List
      grid={{ gutter: 16, column: 4 }}
      pagination={{ pageSize: 40 }}
      dataSource={props.items}
      renderItem={item => (
        <List.Item>
          <ItemGiphy item={item} onItemGiphyClick={props.onItemGiphyClick}/>
        </List.Item>
      )}
    />
  );
}
export default ListGiphy;
