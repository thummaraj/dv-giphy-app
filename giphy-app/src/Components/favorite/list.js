import React, { Component } from 'react'
import ItemFavorite from './item';
import { List } from "antd";

 class  ListFavorite extends Component {
    state = {
        items: []
    }
    componentDidMount(){
        const jsonStr = localStorage.getItem('list-favorite')
        if (jsonStr) {
            const items = JSON.parse(jsonStr)
            console.log('fav', items);
            this.setState({items})
        }
    }
    render() {
    return (
        <List grid={{ gutter: 16, column:4}}
        pagination={{ pageSize: 40}}
        dataSource={this.state.items}
        renderItem={item => (
            <List.Item>
                <ItemFavorite
                item={item}
                onItemGiphyClick={this.props.onItemGiphyClick}/>
            </List.Item>
        )}
        />
    )
        }
}
export default ListFavorite