import React from 'react'
import { Route, Switch , Redirect} from 'react-router-dom'
import ListFavorite from '../Components/favorite/list'
import ListGiphy from '../Components/ListGiphy';

function RouteMenu(props){
    return(
        <Switch>
            <Route
            path="/main"
            exact
            render ={ () => {
                return <ListGiphy items={props.items}
                
                    />
                
            } } 
            />
            <Route path="/favorite" exact component={ListFavorite}/>
            <Redirect from="/*" exact to="/"/>
        </Switch>
    )

}
export default RouteMenu