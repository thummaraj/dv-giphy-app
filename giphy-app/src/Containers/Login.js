import React, { Component } from "react";
import { Button, Form, Input, Icon, message, Divider, Modal } from "antd";
import { auth, provider } from "../firebase";
import { withRouter } from 'react-router-dom';

class Login extends Component {
  state = {
    user: null,
    email: "",
    password: "",
    isLoading: false,
    isShowModal: false,
    isLogin: false
  };
  componentWillMount() {
    // auth.onAuthStateChanged(user => {
    //   this.setState({ user });
    // });
    const jsonStr = localStorage.getItem('user-data');
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }
  onLoginWithFacebook = () => {
    // auth.signInWithPopup(provider).then(({ user }) => {
    //   this.setState({ user });
    //   this.props.history.replace("/main");
    // });
    this.setState({ isLoading: true });
    auth.signInWithPopup(provider).then(({ user }) => {
      this.saveInformationUser(user.email);
    });
  };

  saveInformationUser = email => {
    localStorage.setItem(
      'user-data',
      JSON.stringify({
        email: email,
        isLoggedIn: true
      })
    );
    this.setState({ isLoading: false });
    this.navigateToMainPage();
  };

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push('/main');
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }
  onEmailChange = event => {
    const email = event.target.value;
    this.setState({ email });
  };
  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };
  onSubmitFormLogin = event => {
    event.preventDefault();
    this.setState({ isLogin: true });
    // const isValidEmail = this.validateEmail(this.state.email);
    // const isValidPassword = this.validatePassword(this.state.password);
    // const { history } = this.props;
    // if (isValidEmail && isValidPassword) {
    //   history.push("/main");
    // } else {
    //   message.error("Please try again", 2);
    // }
    const email = this.state.email;
    const password = this.state.password;
    const isEailValid = this.validateEmail(email);
    if (isEailValid) {
      auth
        .signInWithEmailAndPassword(email, password)
        .then(({ user }) => {
          this.setState({ isLogin: false });
          this.saveInformationUser(user.email);
        })
        .catch(_ => {
          this.setState({ isLogin: false });
          this.setState({ isShowModal: true });
        });
    } else {
      this.setState({ isLogin: false });
      message.error('Email or Password invalid!', 1);
    }
  };

  handleCancel = () => {
    this.setState({ isShowModal: false, isLogin: false });
  };

  handleOk = () => {
    this.setState({ isShowModal: false, isLogin: false });
    this.props.history.push('/register');
  };
  render() {
    return (
      <div style={{margin: '60px auto'}}>
        <h2>Welcome To Giphy World</h2>
        <Form onSubmit={this.onSubmitFormLogin}>
          <Form.Item>
            <Input
              style={{ width: "30%", margin: "0 auto" }}
              prefix={<Icon type="user" />}
              placeholder="Email"
              onChange={this.onEmailChange}
            />
          </Form.Item>

          <Form.Item>
            <Input
              style={{ width: "30%", margin: "0 auto" }}
              prefix={<Icon type="lock" />}
              type="password"
              placeholder="Password"
              onChange={this.onPasswordChange}
            />
          </Form.Item>

          <Form.Item>
            <Button 
            style={{ background: '#1DA1F2', color:'white', padding: '5px 58px 10px '}}
            htmlType="submit" >
              Log in
            </Button>
          </Form.Item>

          <Divider />
          <Form.Item>
          <Button
          style={{  background: '#3B5998', color: 'white'}}
          onClick={this.onLoginWithFacebook}>
          Login with Facebook
        </Button>
          </Form.Item>
        </Form>
        <Modal
          title="Something went wrong"
          visible={this.state.isShowModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              No
            </Button>,
            <Button key="submit" type="primary" onClick={this.handleOk}>
              Yes
            </Button>
          ]}
        >
          <p>Sorry! not found this account but you want to create account.</p>
        </Modal>
      </div>
    );
  }
}
export default withRouter(Login);
