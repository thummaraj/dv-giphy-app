import React, { Component } from "react";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {
  Button,
  Modal,Spin,
  Menu,
  Layout,
  message,
  Row,
  Col,
  Icon
} from "antd";
import RouteMenu from "./RouteMenu";
import { connect } from "react-redux";
import { Input } from "antd";


const Search = Input.Search;
const GphApiClient = require("giphy-js-sdk-core");
const client = GphApiClient("H0CSuJCSr29J6Y2Ivb3suoH8AtpPiwOz");
const { Header, Content, Footer } = Layout;
const menus = ["main", "favorite", "logout"];
const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    itemGiphyClick: state.itemGiphyDetail
  };
};

const mapDispathToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: "dismiss_dialog"
      }),
    onItemGiphyClick: item =>
      dispatch({
        type: "click_item",
        payload: item
      })
  };
};

class Main extends Component {
  state = {
    isShowModal: false,
    items: [],
    ItemGif: null,
    pathName: menus[0],
    favItems: [],
    isLoading: false,
  };
  loadDataApi() {
    fetch(
      "http://api.giphy.com/v1/gifs/trending?&api_key=H0CSuJCSr29J6Y2Ivb3suoH8AtpPiwOz&limit=400"
    )
      .then(response => response.json())
      .then(items => this.setState({ items: items.data }));
  }
  componentDidMount() {
    const jsonStr = localStorage.getItem("list-favorite");
    // ถ้า JSON String ออกมาเป็น null พอเอามา parse มันจะไม่ได้ค่า array[]
    //   solution
    const items = JSON.parse(jsonStr) || [];
    this.setState({ favItems: items });

    const { pathname } = this.props.location;
    // pathName = "/main"
    var pathName = menus[0];
    if (pathname != "main") {
      //replace value from "/something" to "something"
      pathName = pathname.replace("/", "");
      //check existing by includes
      if (!menus.includes(pathName)) pathName = menus[0];
    }
    this.setState({ pathName });
    this.loadDataApi()
  }
   
   
  onMenuClick = event => {
    var path = "/";
    if (event.key != "") {
      path = `/${event.key}`;
    }
    this.props.history.replace(path);
  };

  handleCancel = () => {
    this.props.onDismissDialog();
  };
  
  onClickFavorite = () => {
    const itemClick = this.props.itemGiphyClick;
    const items = this.state.favItems;

    const index = items.findIndex(item => {
      return item.title === itemClick.title;
    });
    if (index != -1) {
      items.splice(index,1)
      localStorage.setItem("list-favorite", JSON.stringify(items))
      message.success('unfavorite is successful')
      this.setState({favItems: items})
      this.handleCancel()
      this.loadDataApi()
       } else {
      items.push(itemClick);
      //TODO: save items to localStorage
      localStorage.setItem("list-favorite", JSON.stringify(items));
      message.success("This is a message of success");
      this.setState({ favItems: items})
      this.handleCancel();
    }
  };

  onSearchGiphy = value => {
    console.log(value);
    if (value.trim().length === 0) {
      value = ''
      this.loadDataApi()
      return
  }
  if (value != '' && value != null && value != {}) {
    client
      .search("gifs", { q: value })
      .then(response => {
        if(response.data.length !==0){
          console.log( response.data)
          this.setState({ items: response.data })

      }else{
          message.error('No gif found',1)
          this.loadDataApi()
      }
      
  })
  .catch(() => {

  })
} else {
this.setState({ items: this.state.items })
}

}

  onClickShare = () => {

    const item = this.props.itemGiphyClick;
    navigator.clipboard.writeText(item.images.fixed_width.url)
    message.success("Copied to clipboard", 1);
  };
  showModalConfirmLogout = () => {
    this.setState({ isShowModal: true });
  };
  handleCancellogout = () => {
    this.setState({ isShowModal: false });
  };
  handleOk = () => {
    localStorage.setItem(
      'user-data',
      JSON.stringify({
        isLoggedIn: false
      })
    )
    setTimeout(() => {
      this.setState({ isLoading: false });
      this.props.history.push('/');
    }, 2000);
  };
  onRefresh=()=> {
    console.log('click')
    this.loadDataApi()
    this.props.history.push("/main");
  }
  render() {
    const item = this.props.itemGiphyClick;
    return (
      <div>
        {this.state.items.length > 0 ? (
          <div style={{ height: "100vh" }}>
            {""}

            <Layout className="layout" style={{ background: "white" }}>
              <Header
                style={{
                  padding: "0",
                  position: "fixed",
                  zIndex: 1,
                  width: "100%"
                }}
              >
                <Row
                  type="flex"
                  justify="space-around"
                  align="middle"
                  style={{ background: "white" }}
                >
                
                    <h1 onClick={this.onRefresh} style={{ cursor:'pointer'}}>GiphyWorld</h1>
                
                  <Col>
                    <Search
                      style={{ width: "150%", margin: "15px auto" }}
                      placeholder="input search text"
                      enterButton="Search"
                      size="large"
                      onSearch={this.onSearchGiphy}
                    />
                  </Col>

                  <Col>
                    <Menu
                      theme="white"
                      mode="horizontal"
                      //เพื่อให้มันอยู่ที่เดิม
                      defaultSelectedKeys={[this.state.pathName]}
                      style={{ lineHeight: "64px", marginLeft: "100px" }}
                      onClick={e => {
                        this.onMenuClick(e);
                      }}
                    >
                      <Menu.Item key={menus[0]}><Icon type="home" />Home</Menu.Item>
                      <Menu.Item key={menus[1]}><Icon type="heart"/>Favorite</Menu.Item>
                    </Menu>
                  </Col>
                  <Col>
                  <Button
            type="primary"
            style={{
              background: "red",
              cursor: "pointer",
              marginTop: "10px",
              marginLeft: "60px"
            }}
            onClick={this.showModalConfirmLogout}
          >Logout</Button>
          <Modal
          title="Logout"
          visible={this.state.isShowModal}
          onOk={this.handleOk}
          onCancel={this.handleCancellogout}
        >
          <p>Are you sure to Logout</p>
        </Modal>
                  </Col>
                </Row>
              </Header>
              <Content
                style={{
                  padding: "16px",
                  marginTop: 64,
                  minHeight: "600px",
                  justifyContent: "center",
                  alignItems: "center",
                  display: "flex"
                }}
              >
                <RouteMenu items={this.state.items} />
              </Content>
              <Footer style={{ textAlign: "center", background: "white" }}>
                Giphy Application @ CAMT
              </Footer>
            </Layout>
          </div>
        ) : (
          <Spin size="large" />
        )}
        {item != null ? (
          <Modal
            width="40%"
            style={{ maxHeight: "70%" }}
            title={item.title}
            visible={this.props.isShowDialog}
            onCancel={this.handleCancel}
            footer={[

              <Button
                key="favorite"
                type="primary"
                icon="heart"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}

              />,
              <Button
                key="submit"
                type="primary"
                icon="share-alt"
                size="large"
                shape="circle"
                onClick={this.onClickShare}
              />
            ]}
          >
            {item.images != null ? (
              <img
                src={item.images.fixed_width.url}
                style={{ width: "100%" }}
              />
            ) : (
              <div />
            )}
            {/**/}
            <br />
          </Modal>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispathToProps
)(Main);
